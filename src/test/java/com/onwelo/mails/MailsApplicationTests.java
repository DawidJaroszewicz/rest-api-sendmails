package com.onwelo.mails;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.onwelo.mails.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class MailsApplicationTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    private ResourceFileLoader resourceFileLoader = new ResourceFileLoader();

    @BeforeEach
    private void beforeTest() {
        userRepository.deleteAll();
    }

    @Test
    public void getAllUsers_ok() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_entry.json")))
                .andExpect(status().isCreated());
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user2_entry.json")))
                .andExpect(status().isCreated());

        mvc.perform(get("/users"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is("test")))
                .andExpect(jsonPath("$[0].surname", is("test")))
                .andExpect(jsonPath("$[0].email", is("test@test.com")))
                .andExpect(jsonPath("$[1].name", is("test2")))
                .andExpect(jsonPath("$[1].surname", is("test2")))
                .andExpect(jsonPath("$[1].email", is("test2@test2.com"))).andReturn();
    }

    @Test
    public void getAllUsers_notFound() throws Exception {
        mvc.perform(get("/users"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetUserById_ok() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_entry.json")))
                .andExpect(status().isCreated());

        mvc.perform(get("/users/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("test")))
                .andExpect(jsonPath("$.surname", is("test")))
                .andExpect(jsonPath("$.email", is("test@test.com")));
    }

    @Test
    public void testGetUserById_notFound() throws Exception {
        mvc.perform(get("/users/{id}", 1))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateUser_ok() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_entry.json")))
                .andExpect(status().isCreated());

        mvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("test")))
                .andExpect(jsonPath("$[0].surname", is("test")))
                .andExpect(jsonPath("$[0].email", is("test@test.com")));
    }

    @Test
    public void testCreateUser_badRequest_emptyName() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_empty_name_entry.json")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateUser_badRequest_emptySurname() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_empty_surname_entry.json")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateUser_badRequest_emptyMail() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_empty_mail_entry.json")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateUser_badRequest_badMail() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_bad_mail_entry.json")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testUpdateUser_ok() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_entry.json")))
                .andExpect(status().isCreated());

        mvc.perform(put("/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user2_entry.json")))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());

        mvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("test2")))
                .andExpect(jsonPath("$[0].surname", is("test2")))
                .andExpect(jsonPath("$[0].email", is("test2@test2.com")));
    }

    @Test
    public void testUpdateUser_notFound() throws Exception {
        mvc.perform(put("/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user2_entry.json")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteUser_noContent() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_entry.json")))
                .andExpect(status().isCreated());

        mvc.perform(delete("/users/{id}", 1))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testDeleteUser_notFound() throws Exception {
        mvc.perform(delete("/users/{id}", 1))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSendMails_ok() throws Exception {
        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(resourceFileLoader.getJson("create_user_entry.json")))
                .andExpect(status().isCreated());

        mvc.perform(get("/sendmail"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSendMails_notFound() throws Exception {
        mvc.perform(get("/sendmail"))
                .andExpect(status().isNotFound());
    }
}