package com.onwelo.mails.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private int id;
    @Column(name="name", length = 32)
    private String name;
    @Column(name="surname", length = 32)
    private String surname;
    @Column(name="email", length = 64)
    private String email;
}

