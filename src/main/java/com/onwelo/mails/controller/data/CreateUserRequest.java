package com.onwelo.mails.controller.data;

import lombok.Data;

@Data
public class CreateUserRequest {
    private int id;
    private String name;
    private String surname;
    private String email;
}
