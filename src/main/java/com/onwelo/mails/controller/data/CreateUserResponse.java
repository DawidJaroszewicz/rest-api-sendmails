package com.onwelo.mails.controller.data;

import lombok.Data;

@Data
public class CreateUserResponse {
    private int id;
    private String name;
    private String surname;
    private String email;
}
