package com.onwelo.mails.controller.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TxtReader {
    public static String readTxt(String path) {
        String file_text = "";
        File file = new File(path);
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                file_text += scanner.nextLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + file.toString());
        }
        return file_text;
    }
}
