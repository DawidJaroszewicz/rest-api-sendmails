package com.onwelo.mails.controller;

import com.onwelo.mails.controller.data.EmailValidator;
import com.onwelo.mails.entity.User;
import com.onwelo.mails.repository.UserRepository;
import com.onwelo.mails.service.UserNotFoundException;
import com.onwelo.mails.service.UserService;
import jakarta.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<?> getUsers() throws IOException {
        if (userRepository.count() > 0) {
            List<User> users = userService.getUsers();
            return ResponseEntity.status(HttpStatus.OK).body(users);
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Users do not exist in the database.");
        }
    }

    @GetMapping("/users/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<?> getUser(@PathVariable(name = "id") int id) throws UserNotFoundException {
        if (userRepository.existsById(id)) {
            User user = userService.getUser(id);
            return ResponseEntity.status(HttpStatus.OK).body(user);
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User with given id does not exist.");
        }
    }

    @PostMapping("/users")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<String> addUser(@RequestBody User user) {
        if (!userRepository.existsUserByEmail(user.getEmail())) {
            if (user.getName().isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Insert a name.");
            }
            if (user.getSurname().isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Insert a surname.");
            }
            if (user.getEmail().isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Insert an email.");
            }
            if (EmailValidator.validate(user.getEmail())) {
                userService.addUser(user);
                return ResponseEntity.status(HttpStatus.CREATED).body("New user added.");
            }
            else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This email is incorrect.");
            }
        }
        else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("User with given email already exist.");
        }
    }

    @PutMapping("/users/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<String> updateUser(@PathVariable(name = "id") int id, @RequestBody User newUser)
            throws UserNotFoundException {
        if (userRepository.existsById(id)) {
            userService.updateUser(id, newUser);
            return ResponseEntity.status(HttpStatus.OK).body("User updated.");
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User with given id does not exist.");
        }
    }

    @DeleteMapping("/users/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<String> deleteUser(@PathVariable(name = "id") int id) {
        if (userRepository.existsById(id)) {
            userService.deleteUser(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        }
        else {
          return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User with given id does not exist.");
        }
    }

    @GetMapping("/sendmail")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<String> sendMailToUsers() throws MessagingException, UnsupportedEncodingException {
        if (userRepository.count() > 0) {
            userService.sendMailsToUsers();
            return ResponseEntity.status(HttpStatus.OK).body("Emails sent.");
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot send mails - users do not exist.");
        }
    }
}
