package com.onwelo.mails.service;

import com.onwelo.mails.controller.data.TxtReader;
import com.onwelo.mails.entity.User;
import com.onwelo.mails.repository.UserRepository;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    public List<User> getUsers() {
        return userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public User getUser(int id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    public User addUser(User user) {
        return userRepository.save(user);
    }

    public User updateUser(int id, User newUser) throws UserNotFoundException {
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if (!newUser.getName().isEmpty()) {
            user.setName(newUser.getName());
        }
        if (!newUser.getSurname().isEmpty()) {
            user.setSurname(newUser.getSurname());
        }
        if (!newUser.getEmail().isEmpty()) {
            user.setEmail(newUser.getEmail());
        }
        return userRepository.save(user);
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    public void sendMailsToUsers() throws MessagingException, UnsupportedEncodingException {
        List<User> users = getUsers();
        for (User user : users) {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);

            helper.setFrom("dawidjaroszewicz@wp.pl", "JaroszTeam Support");
            helper.setTo(user.getEmail());

            String subject = "Test";
            String userName = user.getName() + " " + user.getSurname();
            String content = String.format(TxtReader.readTxt("mail_content.txt"), userName);

            helper.setSubject(subject);
            helper.setText(content, true);
            javaMailSender.send(message);
        }
    }
}
