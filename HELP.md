# REST API for sending mails
This project is a simple Spring Boot applicaiton that provides a REST API for CRUD operations on users.

## Requirements
For further reference, please consider the following sections:

* Java 17
* Maven
* any database, like PostgreSQL
* app for using APIs, like Postman

## Installation

1. Clone the repository to your local machine.
2. In the project directory, run `mvn clean install` to build the project.
3. Create a database and configure the connection in the `application.yml` file.
4. Configure the connection to email in the `application.yml` file.
5. Run the project with `mvn spring-boot:run`.

## Usage
The REST API is available with app for using APIs, like Postman. Use the API to manage users by performing CRUD operations.
Available CRUD operations:
* GET /users - get all users from the database
* GET /users/{id} - get user with given id
* GET /sendmail - send mail to all users in the database
* POST /users - add new user to the database
* PUT /users/{id} - update existing user from the database
* DELETE /users/{id} - delete existing user from the database

JSON example with a new user:

```
{
"name": "test",
"surname": "test",
"email": "test@test.pl"
}
```

If you want to change the mail's text, edit `mail_context.txt` file.